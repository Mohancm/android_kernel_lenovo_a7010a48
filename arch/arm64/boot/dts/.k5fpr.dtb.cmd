cmd_arch/arm64/boot/dts/k5fpr.dtb := aarch64-linux-gnu-gcc -E -Wp,-MD,arch/arm64/boot/dts/.k5fpr.dtb.d.pre.tmp -nostdinc -I./arch/arm64/boot/dts -Iarch/arm64/boot/dts -I./arch/arm64/boot/dts/include -I./include/ -I./drivers/of/testcase-data -undef -D__DTS__ -x assembler-with-cpp -o arch/arm64/boot/dts/.k5fpr.dtb.dts.tmp arch/arm64/boot/dts/k5fpr.dts ; ./scripts/dtc/dtc -O dtb -o arch/arm64/boot/dts/k5fpr.dtb -b 0 -i arch/arm64/boot/dts/  -d arch/arm64/boot/dts/.k5fpr.dtb.d.dtc.tmp arch/arm64/boot/dts/.k5fpr.dtb.dts.tmp ; cat arch/arm64/boot/dts/.k5fpr.dtb.d.pre.tmp arch/arm64/boot/dts/.k5fpr.dtb.d.dtc.tmp > arch/arm64/boot/dts/.k5fpr.dtb.d

source_arch/arm64/boot/dts/k5fpr.dtb := arch/arm64/boot/dts/k5fpr.dts

deps_arch/arm64/boot/dts/k5fpr.dtb := \
  arch/arm64/boot/dts/mt6753.dtsi \
    $(wildcard include/config/base.h) \
    $(wildcard include/config/addr.h) \
  arch/arm64/boot/dts/include/dt-bindings/clock/mt6735-clk.h \
  arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/arm-gic.h \
  arch/arm64/boot/dts/include/dt-bindings/interrupt-controller/irq.h \
  arch/arm64/boot/dts/mt6735-pinfunc.h \
  arch/arm64/boot/dts/include/dt-bindings/pinctrl/mt65xx.h \
  arch/arm64/boot/dts/include/dt-bindings/mmc/mt67xx-msdc.h \
  arch/arm64/boot/dts/cust.dtsi \
  arch/arm64/boot/dts/trusty.dtsi \
  arch/arm64/boot/dts/include/dt-bindings/lcm/r63417_fhd_dsi_cmd_truly_nt50358.dtsi \
  arch/arm64/boot/dts/include/dt-bindings/lcm/lcm_define.h \

arch/arm64/boot/dts/k5fpr.dtb: $(deps_arch/arm64/boot/dts/k5fpr.dtb)

$(deps_arch/arm64/boot/dts/k5fpr.dtb):
